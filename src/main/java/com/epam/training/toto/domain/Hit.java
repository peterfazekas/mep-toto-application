package com.epam.training.toto.domain;

import java.math.BigDecimal;

public class Hit {

    private final int hits;
    private final int numberOfGames;
    private final BigDecimal price;

    public Hit(int hits, int numberOfGames, BigDecimal price) {
        this.hits = hits;
        this.numberOfGames = numberOfGames;
        this.price = price;
    }

    public int getHits() {
        return hits;
    }

    public int getNumberOfGames() {
        return numberOfGames;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
