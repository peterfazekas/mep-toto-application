package com.epam.training.toto.domain;

import java.util.Arrays;

public enum Outcome {
    ONE("1", "team #1 won"),
    TWO("2", "team #2 won"),
    DRAW("X", "draw");

    private final String id;
    private final String description;

    Outcome(String id, String description) {
        this.id = id;
        this.description = description;
    }

    public static Outcome getOutcome(final String outcomeId) {
        return Arrays.stream(Outcome.values()).filter(i -> i.getId().equals(outcomeId)).findAny().orElse(null);
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }
}
