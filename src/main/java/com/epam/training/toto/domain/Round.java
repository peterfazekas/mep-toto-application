package com.epam.training.toto.domain;

import java.time.LocalDate;
import java.util.List;

public class Round {

    private final int year;
    private final int week;
    private final Integer round;
    private final LocalDate date;
    private final List<Hit> hits;
    private final List<Outcome> outcomes;

    public Round(Builder builder) {
        this.year = builder.year;
        this.week = builder.week;
        this.round = builder.round;
        this.date = builder.date;
        this.hits = builder.hits;
        this.outcomes = builder.outcomes;
    }

    public int getYear() {
        return year;
    }

    public int getWeek() {
        return week;
    }

    public Integer getRound() {
        return round;
    }

    public LocalDate getDate() {
        return date;
    }

    public List<Hit> getHits() {
        return hits;
    }

    public List<Outcome> getOutcomes() {
        return outcomes;
    }


    /**
     * Builder for {@link Round}
     */
    public static final class Builder {

        private int year;
        private int week;
        private Integer round;
        private LocalDate date;
        private List<Hit> hits;
        private List<Outcome> outcomes;

        public Builder() {
        }

        public Builder withYear(int year) {
            this.year = year;
            return this;
        }

        public Builder withWeek(int week) {
            this.week = week;
            return this;
        }

        public Builder withRound(Integer round) {
            this.round = round;
            return this;
        }

        public Builder withDate(LocalDate date) {
            this.date = date;
            return this;
        }

        public Builder withHits(List<Hit> hits) {
            this.hits = hits;
            return this;
        }

        public Builder withOutcomes(List<Outcome> outcomes) {
            this.outcomes = outcomes;
            return this;
        }

        public Round build() {
            return new Round(this);
        }
    }
}
