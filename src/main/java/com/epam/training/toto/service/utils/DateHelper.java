package com.epam.training.toto.service.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateHelper {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy.MM.dd.");

    public static LocalDate getDate(String text) {
        return text.length() == 0 ? null : LocalDate.parse(text, DATE_TIME_FORMATTER);
    }

}
