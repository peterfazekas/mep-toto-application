package com.epam.training.toto.service.data;

import java.util.List;

public class DataApi {

    private final DataReader file;
    private final DataParser data;

    public DataApi(final DataReader file, final DataParser data) {
        this.file = file;
        this.data = data;
    }

    public <T> List<T> getData(final String source) {
        return data.parse(file.read(source));
    }
}
