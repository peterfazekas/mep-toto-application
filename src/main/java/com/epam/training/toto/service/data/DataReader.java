package com.epam.training.toto.service.data;

import java.util.List;

public interface DataReader {

    List<String> read(String source);

}
