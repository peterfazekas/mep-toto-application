package com.epam.training.toto.service.data;

import com.epam.training.toto.domain.Hit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class HitListParser implements DataParser<Hit> {

    private static int countOfHits = 14;

    @Override
    public List<Hit> parse(List<String> lines) {
        List<Hit> hits = new ArrayList<>();
        for (int i = 0; i < lines.size() - 1; i += 2) {
            hits.add(createHit(lines.get(i), lines.get(i + 1)));
        }
        return hits;
    }

    private Hit createHit(String numberOfGames, String price) {
        String s = price.replaceAll("[^\\d.]+", "");
        return new Hit(countOfHits--, Integer.parseInt(numberOfGames), new BigDecimal(s));
    }
}
