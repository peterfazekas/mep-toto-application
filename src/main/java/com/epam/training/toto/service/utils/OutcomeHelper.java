package com.epam.training.toto.service.utils;

import com.epam.training.toto.domain.Outcome;

import java.util.List;
import java.util.stream.IntStream;

public class OutcomeHelper {

    public static int countHits(final String outcome, final List<Outcome> outcomeList) {
        return (int) IntStream.range(0, outcomeList.size()).filter(i -> isSameOutcome(i, outcome, outcomeList)).count();
    }

    private static boolean isSameOutcome(int index, String outcome, List<Outcome> outcomeList) {
        return isValid(index, outcome, outcomeList) && isEqual(index, outcome, outcomeList);
    }

    private static boolean isEqual(int index, String outcome, List<Outcome> outcomeList) {
        return String.valueOf(outcome.charAt(index)).equals(outcomeList.get(index).getId());
    }

    private static boolean isValid(final int index, final String outcome, final List<Outcome> outcomeList) {
        return outcome.length() >= index && outcomeList.get(index) != null;
    }

}
