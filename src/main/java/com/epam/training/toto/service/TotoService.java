package com.epam.training.toto.service;

import com.epam.training.toto.domain.Hit;
import com.epam.training.toto.domain.Outcome;
import com.epam.training.toto.domain.Round;
import com.epam.training.toto.service.utils.OutcomeHelper;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class TotoService {

    private static final NumberFormat CURRENCY_INSTANCE = NumberFormat.getCurrencyInstance(new Locale("hu", "HU"));
    private static final int OUTCOMES_COUNT = 14;

    private final List<Round> rounds;

    public TotoService(List<Round> rounds) {
        this.rounds = rounds;
    }

    public String getLargestPrice() {
        return transformCurrency(getLargestPriceAmount());
    }

    public String getAggregatedDistributionOfOutcomesOfAllRounds() {
        return Arrays.stream(Outcome.values())
                .map(this::getAggregatedDistributionOfOutcome)
                .collect(Collectors.joining(", "));
    }

    public String getHitsAndAmountForTheWagerByDateAndOutcomes(final LocalDate date, final String outcomes) {
        String result;
        if (getHitByDateAndOutcomes(date, outcomes).isPresent()) {
            Hit hit = getHitByDateAndOutcomes(date, outcomes).get();
            result = String.format("hits: %d, amount: %s", hit.getHits(), transformCurrency(hit.getPrice()));
        } else {
            result = "No valid entry found!";
        }
        return result;
    }

    private String transformCurrency(final BigDecimal price) {
        return CURRENCY_INSTANCE.format(price);
    }

    private BigDecimal getLargestPriceAmount() {
        return rounds.stream()
                .map(Round::getHits)
                .flatMap(Collection::stream)
                .map(Hit::getPrice)
                .max(Comparator.comparingInt(BigDecimal::intValue))
                .get();
    }

    private Optional<Hit> getHitByDateAndOutcomes(final LocalDate date, final String outcomes) {
        return rounds.stream()
                .filter(i -> i.getDate().equals(date))
                .map(Round::getHits)
                .flatMap(Collection::stream)
                .filter(i -> i.getHits() == getMaximumHitsByDateAndOutcomes(date, outcomes))
                .findAny();

    }

    private int getMaximumHitsByDateAndOutcomes(final LocalDate date, final String outcomes) {
        return rounds.stream()
                .filter(i -> i.getDate() != null && i.getDate().equals(date))
                .mapToInt(i -> OutcomeHelper.countHits(outcomes, i.getOutcomes()))
                .max()
                .getAsInt();
    }

    private String getAggregatedDistributionOfOutcome(final Outcome outcome) {
        return String.format("%s: %5.2f %%", outcome.getDescription(), getPercentage(outcome));
    }

    private double getPercentage(final Outcome outcome){
        long outcomeCount = getOutcomeCount(outcome);
        long totalCount = rounds.size() * OUTCOMES_COUNT;
        return outcomeCount * 100.0 / totalCount;
    }

    private long getOutcomeCount(final Outcome outcome) {
        return rounds.stream()
                .map(Round::getOutcomes)
                .flatMap(Collection::stream)
                .filter(outcome::equals)
                .count();
    }

}
