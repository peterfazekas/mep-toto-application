package com.epam.training.toto.service.data;

import com.epam.training.toto.domain.Outcome;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class OutcomeListParser implements DataParser<Outcome> {

    @Override
    public List<Outcome> parse(List<String> lines) {
        return lines.stream()
                .map(this::createOutcome)
                .collect(Collectors.toList());
    }

    private Outcome createOutcome(String item) {
        return Outcome.getOutcome(item.substring(item.length() - 1));
    }
}
