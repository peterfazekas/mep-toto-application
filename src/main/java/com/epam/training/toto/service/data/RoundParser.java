package com.epam.training.toto.service.data;

import com.epam.training.toto.domain.Round;
import com.epam.training.toto.service.utils.DateHelper;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class RoundParser implements DataParser<Round> {

    private static final String SEPARATOR = ";";
    private static final String NO_ENTRY = "-";

    private final DataParser hitParser;
    private final DataParser outcomeParser;

    public RoundParser(DataParser hitParser, DataParser outcomeParser) {
        this.hitParser = hitParser;
        this.outcomeParser = outcomeParser;
    }

    @Override
    public List<Round> parse(List<String> lines) {
        return lines.stream().map(this::createRound).collect(Collectors.toList());
    }

    private Round createRound(String line) {
        String[] items = line.split(SEPARATOR);
        return new Round.Builder()
                .withYear(Integer.parseInt(items[0]))
                .withWeek(Integer.parseInt(items[1]))
                .withRound(getRound(items[2]))
                .withDate(DateHelper.getDate(items[3]))
                .withHits(hitParser.parse(getItems(items, 4, 13)))
                .withOutcomes(outcomeParser.parse(getItems(items, 14, 28)))
                .build();
    }

    private List<String> getItems(String[] items, int from, int to) {
        return Arrays.asList(Arrays.copyOfRange(items, from, to));
    }

    private Integer getRound(String text) {
        return NO_ENTRY.equals(text) ? null : Integer.parseInt(text);
    }

}
