package com.epam.training.toto.service.data;

import com.epam.training.toto.service.utils.DateHelper;

import java.time.LocalDate;
import java.util.Scanner;

public class Console {

    private final Scanner scanner;

    public Console(final Scanner scanner) {
        this.scanner = scanner;
    }

    public LocalDate readDate(final String text) {
        System.out.print(text);
        return DateHelper.getDate(scanner.next());
    }

    public String readOutcomes(final String text) {
        System.out.print(text);
        return scanner.next();
    }

}
