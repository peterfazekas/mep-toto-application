package com.epam.training.toto;

import com.epam.training.toto.service.TotoService;
import com.epam.training.toto.service.data.*;

import java.time.LocalDate;
import java.util.Scanner;

public class App {

    private static final String SOURCE = "src\\main\\resources\\toto.csv";

    private final TotoService totoService;
    private final Console console;

    public App() {
        DataApi data = new DataApi(new FileDataReader(), new RoundParser(new HitListParser(), new OutcomeListParser()));
        totoService = new TotoService(data.getData(SOURCE));
        console = new Console(new Scanner(System.in));
    }

    public static void main(String[] args) {
        App app = new App();
        app.run();
    }

    private void run() {
        System.out.println("The largest prize ever recorded is " + totoService.getLargestPrice());
        System.out.println("The aggregated distribution of the 1/2/X results of all rounds are " + totoService.getAggregatedDistributionOfOutcomesOfAllRounds());
        LocalDate date = console.readDate("Enter Date: ");
        String outcomes = console.readOutcomes("Enter outcomes (1/2/X): ");
        System.out.println("Result: " + totoService.getHitsAndAmountForTheWagerByDateAndOutcomes(date, outcomes));
    }
}
